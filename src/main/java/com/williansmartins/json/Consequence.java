package com.williansmartins.json;

public enum Consequence {
	MUITOS_ERROS, ERRO, SUCESSO, AVISO, RELATORIO, RETRY, STOP_RETRY, REDIRECT;
}
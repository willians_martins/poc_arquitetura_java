package com.williansmartins.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.williansmartins.entity.Classe;
import com.williansmartins.entity.Entidade;
import com.williansmartins.json.Consequence;
import com.williansmartins.json.JSONReturn;

/**
 * Servlet implementation class ServletPrincipal
 */
@WebServlet(value="/servletPrincipal")
public class ServletPrincipal extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
    /**
     * Default constructor. 
     */
    public ServletPrincipal() {
        System.out.println("nasceu");
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("application/json");
		response.setHeader("Cache-Control", "nocache");
        	response.setCharacterEncoding("utf-8");
		PrintWriter out = response.getWriter();

		JSONReturn jsonReturn;
		Entidade e = new Entidade();
		e.idade = 12;
		e.nome = "nayara";
		
		List<Entidade> lista = new ArrayList<Entidade>();
		lista.add(e);
		lista.add(e);
		jsonReturn = JSONReturn.newInstance(Consequence.SUCESSO, lista);

		// finally output the json string		
		out.print(jsonReturn.serialize());
		out.flush();
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String classe = request.getParameter("classe");
		String metodo = request.getParameter("metodo");
		System.out.println(">>" + classe);
		System.out.println(">>" + metodo);
		
		Entidade entidade = null;
		response.setContentType("application/json");
		response.setHeader("Cache-Control", "nocache");
        	response.setCharacterEncoding("utf-8");
		PrintWriter out = response.getWriter();
		
		try {
			Class<?> c = Class.forName("com.williansmartins.entity.Classe");
			Classe instancia = (Classe)c.newInstance();
			Method method = c.getClass().getMethod( metodo );  
//			Method method = c.getDeclaredMethod(metodo, new Class<?>[0]);
//			Method method = Classe.class.getMethod(metodo, ServletRequest.class, ServletResponse.class);
			method.invoke(instancia, metodo);
		} catch (Exception e) {
			e.printStackTrace();
		}
		JSONReturn jsonReturn = JSONReturn.newInstance(Consequence.SUCESSO, entidade);
		out.print(jsonReturn.serialize());
		out.flush();
	}

}

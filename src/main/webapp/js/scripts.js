var classe = new function(){
	this.chamarAjax = function(){
		$.ajax({
			type:"GET",
			url:"/poc-java-arquitetura/servletPrincipal",
			data: ({classe : "nomeDaClasse", metodo:"metodo"}),
			dataType:"json",
			success: function(jsonReturn){
				var consequence = jsonReturn.consequence;
				if (consequence == "ERRO"){
					alert(jsonReturn);
				}else if (consequence == "SUCESSO"){
					var itens = jsonReturn.dado;
					jQuery.each(itens, function(index, value) {
				       console.log(this.nome);
				       //return (this != "three"); // will stop running after "three"
				   });
				}
			}
		});
	}
	
	this.acionarClasseJava = function(){
		$.ajax({
			type:"POST",
			url:"/poc-java-arquitetura/servletPrincipal",
			data: ({classe : "nomeDaClasse", metodo:"metodo2"}),
			dataType:"json",
			success: function(jsonReturn){
				var consequence = jsonReturn.consequence;
				if (consequence == "ERRO"){
					alert(jsonReturn);
				}else if (consequence == "SUCESSO"){
					console.log(jsonReturn.dado);
				}
			}
		});
	}
};

$(document).ready(function(){
	$("#botao_ajax1").click(function(){
		classe.chamarAjax();
	});
	$("#botao_ajax2").click(function(){
		classe.acionarClasseJava();
	});
});